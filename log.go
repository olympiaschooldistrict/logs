package logs // Package logs import "bitbucket.org/olympiaschooldistrict/logs"

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"sync"

	"cloud.google.com/go/errorreporting"
	"cloud.google.com/go/logging"
)

// Client MUST be closed for logs to be delivered properly
type Client struct {
	*logging.Logger
	lc           *logging.Client
	errorClient  *errorreporting.Client
	AlsoStderr   bool
	otherOutputs []io.Writer
}

// NewClient returns a Client for our logging and error reporting purposes
func NewClient(ctx context.Context, projectID, logfile string, opts ...logging.LoggerOption) (c *Client, err error) {
	lc, err := logging.NewClient(ctx, projectID)

	if err != nil {
		return nil, fmt.Errorf("failed to create logging client: %v", err)
	}
	err = lc.Ping(ctx)
	if err != nil {
		return nil, fmt.Errorf("can't reach log service: %v", err)
	}

	ec, err := errorreporting.NewClient(ctx, projectID, errorreporting.Config{
		ServiceName: logfile,
		OnError: func(err error) {
			log.Printf("Could not log error: %v", err)
		},
	})
	if err != nil {
		return nil, err
	}

	c = &Client{
		lc.Logger(logfile, opts...),
		lc,
		ec,
		false,
		[]io.Writer{},
	}

	return c, nil
}

var done = map[logging.Severity]bool{}
var doneMu = sync.RWMutex{}

func (c *Client) stdLogger(level logging.Severity) *log.Logger {
	l := c.StandardLogger(level)
	doneMu.RLock()
	if done[level] {
		doneMu.RUnlock()
		return l
	}
	doneMu.RUnlock()
	doneMu.Lock()
	writers := c.otherOutputs
	writers = append(writers, l.Writer())
	if c.AlsoStderr {
		writers = append(writers, os.Stderr)
	}
	l.SetOutput(io.MultiWriter(writers...))
	done[level] = true
	doneMu.Unlock()
	return l
}

func (c *Client) AdditionalLogOutputs(writers ...io.Writer) {
	c.otherOutputs = writers
}

// Info returns a standard logger of Info level severity
func (c *Client) Info() *log.Logger {
	return c.stdLogger(logging.Info)
}

// Warning returns a standard logger of Warning level severity
func (c *Client) Warning() *log.Logger {
	return c.stdLogger(logging.Warning)
}

// Debug returns a standard logger of Debug level severity
func (c *Client) Debug() *log.Logger {
	return c.stdLogger(logging.Debug)
}

// Error returns a standard logger of Error level severity
func (c *Client) Error() *log.Logger {
	return c.stdLogger(logging.Error)
}

// Close closes the sub-clients
func (c *Client) Close() error {
	lErr := c.lc.Close()
	eErr := c.errorClient.Close()
	if lErr != nil {
		return lErr
	}
	if eErr != nil {
		return eErr
	}
	return nil
}

func (c *Client) ReportIfError(err error) {
	if err != nil {
		c.ReportError(err)
	}
}

// ReportError sends an errormessage to Stackdriver and logs it to logging as an Error level severity
func (c *Client) ReportError(err error) {
	c.errorClient.Report(errorreporting.Entry{
		Error: err,
	})
	c.Log(logging.Entry{
		Severity: logging.Error,
		Payload:  err.Error(),
	})
}

// ReportErrorf builds your own errormessage, sends to Stackdriver and logs it to logging as an Error level severity
func (c *Client) ReportErrorf(format string, args ...interface{}) {
	err := fmt.Errorf(format, args...)
	c.errorClient.Report(errorreporting.Entry{
		Error: err,
	})
	c.Log(logging.Entry{
		Severity: logging.Error,
		Payload:  err.Error(),
	})
}
